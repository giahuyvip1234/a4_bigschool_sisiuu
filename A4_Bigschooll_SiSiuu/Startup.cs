﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(A4_Bigschooll_SiSiuu.Startup))]
namespace A4_Bigschooll_SiSiuu
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
